import React, {Component} from 'react';


import Number from './Number';



class Numbers extends Component {

    state = {
        rand_numbers: this.get_random()
    };

    get_random() {
        const min = 5;
        const max = 36;
        const rand_numbers = [];
        while(rand_numbers.length !== 5) {
            const rand_number = Math.floor(Math.random() * (max - min + 1)) + min;;
            if (!rand_numbers.includes(rand_number)) {
                rand_numbers.push(rand_number);
            }
        }
        rand_numbers.sort(function(a,b) {return a-b});
        return rand_numbers;
    }

    changeNumbers = () => {
        this.setState({
            rand_numbers: this.get_random()
        })
    }

    render() {
        return (
            <div>
                <div className="numbers-div">
                    {this.state.rand_numbers.map(rand_number => {
                        return (
                            <Number key={rand_number} number={rand_number}/>
                        );
                    })}
                </div>
                <div>
                    <button onClick={this.changeNumbers} className="btn-change-numbers">New numbers</button>
                </div>
            </div>
        );
    }
}

export default Numbers;
