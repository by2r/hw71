import React from 'react';

export default function Number(props) {
    return (
        <div className="number-div">
            <h1 className="number">{props.number}</h1>
        </div>
    );
}